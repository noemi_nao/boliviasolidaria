# Portal boliviasolidaria.org.bo

Página de bolivia solidaria (boliviasolidaria.org.bo).

BoliviaSolidaria es una plataforma para gestionar acciones de colaboración entre ciudadanos desde cualquier lugar de Bolivia.

## Sobre el proyecto

### ¿Quiénes pueden usar la plataforma? 

La plataforma puede ser usada por cualquier persona con acceso a internet y desde cualquier dispositivo, que requiera u ofrezca ayuda. Una vez enviada la solicitud, pasará a un proceso de revisión para luego ser publicada en la página. Aquí el solicitante debe ser lo más específico con su necesidad u oferta de ayuda .

### Por qué hacemos voluntariado en Bolivia Solidaria?

Durante los últimos años en Bolivia hemos vividos diferentes tipos de sucesos como desastres naturales, deslizamientos, sequías etc. En tiempos de crisis, es necesario contar con ayuda oportuna y queremos aportar en la construcción de una red que lo permita.

Muchas personas pueden brindar ayuda, pero no saben cómo ni donde, mediante la plataforma podrán ver las necesidades en las que pueden focalizar su ayuda y ser parte de la solución.

Asimismo, los Organismos de ayuda humanitaria necesitan una plataforma que les facilite información para distribuir la ayuda y llegar a los damnificados de manera oportuna.


### ¿Quiénes pueden usar la plataforma? 
La plataforma puede ser usada por cualquier persona con acceso a internet y desde cualquier dispositivo, que requiera u ofrezca ayuda. Una vez enviada la solicitud, pasará a un proceso de revisión para luego ser publicada en la página. Aquí el solicitante debe ser lo más específico con su necesidad u oferta de ayuda .
 
¿Qué se puede hacer en la página?
En la página https://boliviasolidaria.org se pueden realizar reportes de:

- Solicitudes de ayuda
- Ofertas de ayuda 
- Servicios de salud
- Servicios públicos disponibles

## Sobre el desarrollo del sitio web

El sitio se conecta al servicio de [ushaidi](https://www.ushahidi.com/), este servicio es una plataforma donde se registra información como solicitudes y ofertas de ayuda, servicios de salud y otros servicios públicos. El servicio de ushaidi tiene un mapa y permite llenar un formulario con solicitudes, estas solicitudes van a una bandeja donde es revisada por las personas que administran la cuenta de ushaidi.

La página es estática y tiene un iframe que muestra el mapa de ushaidi correspondiente a boliviasolidaria, pero el [api de ushaidi](https://docs.ushahidi.com/platform-developer-documentation/tech-stack/api-documentation) esta bien documentado y se podría usar en un frontend propio. 

### Sobre la migración del servicio de ushaidi

El servicio de ushaidi es de pruebas y expirará en unas semanas, se puede instalar una instancia de ushaidi en un servidor de nuestro control y asegurar la permanencia del servicio.

### Instalación

Solo hay que clonar este proyecto y abrir el `index.html` en un navegador.

### Tareas pendientes

#### Apariencia e interfaz

- Revisar la distribución del navbar superior.
- Corregir estilos css, por ejemplo en el dropdown del navbar al poner el cursor encima de una opción las letras se colorean de blanco y no se puede leer el texto.
- Completar las páginas informativas como acerca de, voluntarios, como aportar y otros.
- Revisar hojas de estilo y archivos javascript no usados, sería bueno quitarlos para agilizar la carga de la página.

#### Infraestructura

- Diseñar una interfaz que asegure alta disponibilidad.

#### Noticias

Se tiene planificado incluir un pequeño backend para mostrar publicaciones y que se puedan hacer comentarios.

